#!/bin/bash

while read line; do
  # collects request ids  
  [ "${line:0:7}" == "request" ] && 
    req_id="$(awk -F= '{ print $2 }' <<< "$line")";
  # checks if content type is heroku
  [ "${line:0:8}" == "response" ] && 
    [ "heroku/content" == "$(awk '{ print $3 }' <<< "$line")" ] && 
    found="true"

  # if looking at fwd
  if [ "${line:0:3}" == "fwd" ]; then
    # if it's a heroku fwd
    if [ "$found" == "true" ]; then
      # grab fwd value
      fwd_value="$(awk -F= '{ print $2 }' <<< "$line")";
      # if that value is masked
      if [ "$fwd_value" == "MASKED" ]; then
        echo "$req_id [M]"
        else
          echo "$req_id"  
      fi
    fi
    # reset found to false till we find another req which is type heroku
    found="false"
  fi

done < "/dev/stdin"

